//
//  Colors.swift
//  MarvelList
//
//  Created by Eduardo Jordan on 9/5/21.
//

import UIKit

struct Colors {
    
    static var MarvelBlues: UIColor = UIColor.init(hexString: "#0B89FF")!
    static var MarvelWhite: UIColor = UIColor.init(hexString: "#FFFFFF")!
    static var MarvelYellow: UIColor = UIColor.init(hexString: "#F9CD5F")!
    static var MarvelBlack: UIColor = UIColor.init(hexString: "#000000")!
    static var MarvelRed: UIColor = UIColor.init(hexString: "#ff0000")!
    
}

