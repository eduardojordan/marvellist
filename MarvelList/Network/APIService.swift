//
//  APIService.swift
//  MarvelList
//
//  Created by Eduardo Jordan on 8/5/21.
//

import UIKit

class APIService {
    
    func apiToGetCharacterData(page: String, completion : @escaping (Result<Characters, NetworkError>) -> ()){
        let sourcesURL = URL(string: ApiURL.shared.basePath + "limit=\(ApiURL.shared.limit)&offset=\(page)&" + ApiURL.shared.getCredentials())
        URLSession.shared.dataTask(with: sourcesURL!) { (data, urlResponse, error) in
            if let data = data {
                let jsonDecoder = JSONDecoder()
                let character = try! jsonDecoder.decode(Characters.self, from: data)
                completion(.success(character))
            } else {
                completion(.failure(.genericError))
            }
        }.resume()
    }
    
}

