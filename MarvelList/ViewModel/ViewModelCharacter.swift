//
//  ViewModelCharacter.swift
//  MarvelList
//
//  Created by Eduardo Jordan on 8/5/21.
//


import UIKit

class ViewModelCharacter {
    
    var refreshData = { () -> () in }
    var characterData : [DataCharacter] = [] {
        didSet {
            refreshData()
        }
    }
    
    func retrieveData(page:String) {
        let apiService = APIService()
        apiService.apiToGetCharacterData(page: page) { characterData in
            switch characterData {
            case .success (let character):
                self.characterData.append(contentsOf: character.data!.results)
            case .failure(let error):
                if (error == .specificError) {
                    print(error.localizedDescription)
                } else {
                    print(NetworkError.genericError)
                }
            }
        }
    }
    
}
