//
//  CharactersViewController.swift
//  MarvelList
//
//  Created by Eduardo Jordan on 8/5/21.
//

import UIKit

class CharactersViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    static var shared = CharactersViewController()
    
    var viewModel = ViewModelCharacter()
    var page = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        setupTableView()
        configureView()
        bind()
    }
    
    func setupNavBar() {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 38, height: 20))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "LogoMarvel")
        imageView.image = image
        navigationItem.titleView = imageView
    }
    
    func setupTableView() {
        tableView.rowHeight = 250
        tableView.backgroundColor = UIColor.black
    }
    
    func configureView() {
        activity.startAnimating()
        activity.isHidden = false
        viewModel.retrieveData(page: String(page))
    }
    
    func bind() {
        viewModel.refreshData = { [weak self] () in
            DispatchQueue.main.async { [self] in
                self!.tableView.reloadData()
                self!.activity.stopAnimating()
                self?.activity.isHidden = true
            }
        }
    }
    
    func loadMoreData() {
        activity.startAnimating()
        activity.isHidden = false
        page += 100
        viewModel.retrieveData(page:String(page))
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height
        {
            self.loadMoreData()
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.activity.stopAnimating()
                self.activity.isHidden = true
            }
        }
    }
}
