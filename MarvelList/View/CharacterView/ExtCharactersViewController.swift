//
//  ExtCharactersViewController.swift
//  MarvelList
//
//  Created by Eduardo Jordan on 8/5/21.
//

import UIKit

// MARK: - UITableViewDataSource

extension CharactersViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.characterData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellCharacters") as! CellCellCharactersViewController
        
        let marvelHero = viewModel.characterData[indexPath.row]
        
        cell.selectionStyle = .none
        cell.nameCharacters.text = marvelHero.name

        let imgData = "\(marvelHero.image!)" + "/portrait_xlarge.jpg"
        let url = URL(string: imgData)
        
        if (url == nil || imgData.contains("image_not_available")) {
            print("imgData",imgData)
            cell.imgChracters?.image = UIImage(named: "ImageNotAvailable2")
            cell.imgChracters?.contentMode = .scaleAspectFill
        } else {
            cell.imgChracters?.image = UIImage(url: URL(string: imgData))
            cell.imgChracters?.contentMode = .scaleAspectFill
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex && viewModel.characterData.count > 0 {
            let spinner = UIActivityIndicatorView(style: .medium)
            spinner.color = Colors.MarvelRed
            spinner.frame = CGRect(x: 0.0, y: 0.0, width: tableView.bounds.width, height: 70)
            spinner.startAnimating()
            tableView.tableFooterView = spinner
        }
    }
}

// MARK: - UITableViewDelegate

extension CharactersViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let marvelHero = viewModel.characterData[indexPath.row]
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        controller.getName = marvelHero.name!
        controller.getDescription = marvelHero.description!
        controller.getImage = marvelHero.image!
        self.navigationController!.pushViewController(controller, animated: true)
        
    }
    
}
