//
//  CellCellCharactersViewController.swift
//  MarvelList
//
//  Created by Eduardo Jordan on 8/5/21.
//

import UIKit

class CellCellCharactersViewController: UITableViewCell {
 
    @IBOutlet weak var imgChracters: UIImageView!
    @IBOutlet weak var nameCharacters: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.nameCharacters.textColor = UIColor.white
        self.nameCharacters.font = UIFont(name: "Antonio-Bold", size: 22)
    }

}

