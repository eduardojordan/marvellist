//
//  ExtDictionary.swift
//  MarvelList
//
//  Created by Eduardo Jordan on 8/5/21.
//

import Foundation

extension Dictionary {
    var queryString: String? {
        return self.reduce("") { "\($0!)\($1.0)=\($1.1)&" }
    }
}
