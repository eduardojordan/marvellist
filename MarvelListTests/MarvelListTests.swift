//
//  MarvelListTests.swift
//  MarvelListTests
//
//  Created by Eduardo Jordan on 8/5/21.
//

import XCTest
@testable import MarvelList

class MarvelListTests: XCTestCase {
    
    func testParseJsonMock() throws {
        guard let pathString = Bundle(for: type(of: self)).path(forResource: "marvel", ofType: "json") else {
            fatalError("json not found")
        }
        guard let json = try? String(contentsOfFile: pathString, encoding: .utf8) else {
            fatalError("Unable to convert jsont to String")
        }
        let jsonData = json.data(using: .utf8)!
        let character = try! JSONDecoder().decode(Characters.self, from: jsonData)
        
        XCTAssertEqual("3-D Man", character.data?.results[0].name)
        XCTAssertEqual("", character.data?.results[0].description)
    }
    
    func testCallAPISuccess() throws {
        let sourcesURL = URL(string: ApiURL.shared.basePath + "limit=\(ApiURL.shared.limit)&offset=0&" + ApiURL.shared.getCredentials())!
        var request = URLRequest(url: sourcesURL)
        request.httpMethod = "GET"
        request.setValue("application/json; charset=utf8", forHTTPHeaderField: "Content-Type")
        let promise = expectation(description: "Call API success")
        
        URLSession.shared.dataTask(with: request) {(data, response,error) in
            guard let _ = data, error == nil, let result = response as? HTTPURLResponse else {
                return
            }
            do{
                XCTAssertTrue(result.statusCode == 200)
                promise.fulfill()
            }
        }.resume()
        waitForExpectations(timeout: 5, handler: nil)
    }
}
