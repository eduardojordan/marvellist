![image appMarvel](MarvelCapture.jpg)

# Marvel List

It is a practice of basic app which makes use of the Marvel API to recreate and create projects based on the best super heroes of Marvel.
This app show a list of Marver character and detail of super hero


This project is carried out:

* Swift = v5.0
* Xcode = v12.5
* iOS 14.3
* Design pattern MVVM
* include UnitTest
* include Localization
* include pagination
* include extra fonts
* SwiftLint
* include cocoaPods


#### Software used
* Adobe Illustrator
* Icon Set Creator
* terminal
* sublime Text
* Postman
* Xcode
* SourceTree
* Swift

APIMARVEL: https://developer.marvel.com

* Info: Not all characters in API have a description and photo

Note: I like to improve every day that is why I would appreciate any comment that contributes to being a better professional :] 
Thanks!

